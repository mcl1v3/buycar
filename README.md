## About Project

Must use technologies:\

● Laravel Framework\
● jQuery – Ajax\
● CSS3\
● Search functionality must be done through ElasticSearch\
● MySql\

Task:\

Imagine a client has asked us to create a basket where users can purchase cars online. Please NOTE that the candidate should NOT use any Laravel pre-build plugins to complete this task. All code must be done from scratch.\

Front End - User Rules:\

● Login Form\
● Users should be logged in before buying\
● Users should be able to add to the basket even if they are not logged in but not be able to buy anything. So even if they are not buying anything but have added cars to their basket and then decide to register those cars should be added to that user.\
● User should be able to add/Delete cars from the basket\
● User should be able to change the quantity of the cars in the basket\
● Search (USE ELASTIC SEARCH) – On home page user should be able to search for cars – by name, engine size.\

Backend – Administration rules\

● Administration login and Dashboard\
● Admin – Able to add cars and make them active so they appear on the frontend\
● Details of the cars – Make – Model – Registration – Engine Size – Price\
● Categorize the cars – so each car has tags i.e. red – 5 door\

Viewing the code, Please send us a link to a website where you can make this public or send us the code and instructions how to set it up.\

## START PROJECT

## MIGRATE DATABASE, FIRST CONFIGURE .ENV

AFTER EXECUTE: `php artisan migrate`

## SEED

SEED: `php artisan db:seed`

# default user

admin@gmail.com : admin \
mclive.case@gmail.com : admin

## GENERATE OTHERS KEYS AND SECRET

# optional not use jwt

EXECUTE `php artisan key:generate` AND `php artisan jwt:secret`

## FOR FRONT END

EXECUTE FOR INSTALL PACKAGES `yarn install` or `npm install` \
EXECUTE FOR WATCHED CHANGED `yarn watch` or `npm run watch` \
EXECUTE FOR PRODUCTION `yarn prod` or `npm run prod`

## ADITIONAL COMMANDS

SYNC THE SEED DATABASE TO THE ELASTICSEARCH \
`php artisan sync:product`
