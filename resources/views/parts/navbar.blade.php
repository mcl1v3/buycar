<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Cars</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/">Home</a>
                </li>
                @if(auth()->user() && auth()->user()->type_user_id==1)
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/admin">Add Cars</a>
                </li>
                @endif
                
            </ul>
            <form class="d-flex" method="GET" action="/">
                <input class="form-control me-2" type="search" placeholder="Search" name="search" value="{{ request()->get('search') }}" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/cart">
                        <i class="bi bi-bag-fill"></i>&ensp;<span id="cart-counter"></span> items
                    </a>
                </li>
                @if(auth()->user())
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/payments">
                        <i class="bi bi-wallet2"></i>&ensp;Payments
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/logout">
                        <i class="bi bi-box-arrow-left"></i>&ensp;Logout
                    </a>
                </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/login">
                            <i class="bi bi-person-circle"></i>&ensp;Login
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/register">
                            <i class="bi bi-person-plus"></i>

                            &ensp;Register
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>