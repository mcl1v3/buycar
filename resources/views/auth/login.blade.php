
@extends('layouts.base')
@section('title', 'Login form')

@section('content')
<div class="login">
    <div class="card fix-card ">
        <form class="card-text" id="loginForm">
            <div class="text-center">
                <h3>Login</h3>
            </div>
            
            <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email"  required class="form-control" id="inputEmail" placeholder="Email" name="email">
            </div>
            </div>
            <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" required class="form-control" id="inputPassword" placeholder="Password" name="password">
            </div>
            </div>
            <div id="errorsLogin" style="display:none;" class="alert alert-danger" role="alert"></div>
            <div class="text-center">
                <button class="btn btn-success" type="submit">Ingresar</button>
            </div>
        </form>
        
    </div>
</div>
  @endsection