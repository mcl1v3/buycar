@extends('layouts.base')
@section('title', 'Login form')

@section('content')
    <form class="form"  id="register">
        <h2>Registration</h2>

        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" required="" class="form-control" name="name" placeholder="name">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label"> Email</label>
            <div class="col-sm-10">
                <input type="email" required="" class="form-control" name="email" placeholder="Email">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" required="" class="form-control" name="password" placeholder="password">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Confirm Password</label>
            <div class="col-sm-10">
                <input type="password" required="" class="form-control" name="password_confirmation" placeholder="password">
            </div>
        </div>
        <button  class="btn btn-success" name="register" >Register</button>
    </form>
@endsection
