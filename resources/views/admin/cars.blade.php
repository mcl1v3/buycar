@extends('layouts.base')
@section('title', 'Login form')

@section('content')

<div class="text-right" style="text-align:right;">
    <button class="btn btn-success" id="addProduct">Add</button>
</div>
<div style="overflow: auto">
    <table class="table">
        <caption>List of Product</caption>
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Category</th>
                <th scope="col">details</th>
                <th scope="col">tags</th>
                <th scope="col">price</th>
                <th scope="col">cantidad</th>
                <th scope="col">Hidden</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody id="body-table">
        </tbody>
    </table>
</div>
<div class="text-center">
<div id="pagination-container"></div>
</div>

<div class="modal fade" id="modalAddEdit" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Product Cars <span id="productId"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addForm">
                    <strong>Product</strong>
                    <div class="form-group">
                        <input required type="hidden" class="form-control" id="id" name="id"
                            aria-describedby="" placeholder="$$$$">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input required type="text" class="form-control" id="name" name="name"
                            aria-describedby="emailHelp" placeholder="Name Product">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea required class="form-control" aria-label="With textarea" placeholder="description"
                            name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputState">Category</label><br>
                        <div class="fix-select-full">
                            <select required id="selectCategory" class="form-control" name="category_id">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputState">Tags</label><br>
                        <div class="fix-select-full">
                            <select required id="selectTag" class="form-control" name="product_tag[]" multiple>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputState">Details</label><br>
                        <div class="fix-select-full">
                            <select required id="selectDetails" class="form-control" name="product_details[]" multiple>
                            </select>
                        </div>
                    </div>
                    <div id="containerDetails" class="card card-body fix-select-full"></div>
                    <div class="form-group">
                        <label for="name">Price</label>
                        <input required type="number" class="form-control" name="price" aria-describedby="emailHelp"
                            placeholder="$$$$">
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="hidden" id="Check">
                        <label class="custom-control-label" for="Check">Hidden for the home</label>
                      </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-stock" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Stock to Product Cars</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="stockForm">
                    <strong>AddStock</strong>
                    <div class="form-group">
                        <label for="name">Price Buyed</label>
                        <input required type="number" class="form-control" id="name" name="price"
                            aria-describedby="emailHelp" placeholder="Name Product">
                    </div>
                    <div class="form-group">
                        <label for="name">Quantity (this add to the existence)</label>
                        <input required type="number" class="form-control" name="quantity" aria-describedby="emailHelp"
                            placeholder="0">
                    </div>
                    <div class="form-group">
                        <input required type="hidden" class="form-control" id="product_id" name="product_id"
                            aria-describedby="emailHelp" placeholder="$$$$">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection