@extends('layouts.base')
@section('title', 'Login form')

@section('content')
<div style="overflow: auto">
  <table class="table">
    <caption>List of Payment</caption>
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Product</th>
        <th scope="col">Quantity</th>
        <th scope="col">Price</th>
        <th scope="col">Date</th>
      </tr>
    </thead>
    <tbody id="body-table">
    </tbody>
  </table>
</div>

<div class="text-center">
  <div id="pagination-container"></div>
</div>
@endsection