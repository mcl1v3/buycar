@extends('layouts.base')
@section('title', 'Page Title')


<?php
use App\Models\Product;
$products = Product::with(['details.detail', 'tags.tag', 'category'])->get();
?>
@section('content')
    <div class="container">
        <div>
            <form method="GET" target="/">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <strong>Write text</strong><br>
                        <div style="display:flex;">
                        <input class="form-control me-2" type="search" placeholder="Search" name="search"
                            value="{{ request()->get('search') }}" aria-label="Search">
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </div>
                    </div>
                    <!--  <div class="col-sm-12 col-md-4">
                            <strong>Category</strong>
                            <select  style="display:none;" id="selectCategory" class="form-control" name="category_id" placeholder="Select a Category" multiple>
                                <option style="display:none;">Select a Category</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <strong>Detail</strong>
                            <select style="display:none;"  id="selectDetail" class="form-control" name="category_id" placeholder="Select a Details" multiple>
                                <option style="display:none;">Select a Details</option>
                            </select>
                        </div>-->
                    <div class="text-center col-sm-12 col-md-8">
                    </div>
                </div>
            </form>

        </div>
        <div class="row" id="containCard">
        </div>

        <div class="text-center">
            <div id="pagination-container"></div>
        </div>
    </div>
@endsection
