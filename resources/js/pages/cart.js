var jq = window.$;

// to the cart get the cart logic and verify
var cart = localStorage.getItem("cart");
if (cart && cart != "null" && cart != "[]") {
    cart = JSON.parse(cart);
    //function to  generate list cart
    function rendered(data) {
        var html = '<div class="row">';
        var total = 0;
        data.forEach((el1, ind2) => {
            var buttons = `
        			<div class="qty mt-5 clicked-add">
                        <button  class="minus btn btn-sm btn-success"
                         data="${window.jsonToData(
                             el1
                         )}" key="${ind2}">-</button>
                        <span class="count">
                        ${window.getQuantityToProduct(el1.id)}</span>
                        <button class="plus btn btn-sm btn-success"
                        data="${window.jsonToData(
                            el1
                        )}" key="${ind2}">+</button>
                    </div>
        `;
            html += `<div class="card col-sm-12 col-md-4 col-lg-3">
                <img class="card-img-top" src="https://picsum.photos/200/100?random=${
                    ind2 + 1
                }" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">
                        ${el1["name"]}
                    </h5>
                    <h6>Price: $  ${el1["price"]} </h6>
                    <p class="card-text">
                      Quantity:   ${el1["quantity"]}
                    </p>
                    <p class="card-text">
                      SubTotal:   $ ${el1["price"] * el1["quantity"]}
                    </p>
                    <span>
                    ${buttons}
                    </span>
                </div>
            </div>`;
            total += el1["price"] * el1["quantity"];
        });
        html += `</div><div class="text-center">Total $ ${total}</div>`;
        jq("#cartList").html(html);
        jq("#buttonBuy").on("click", () => {
            jq("#load-container").css({ display: "initial" });
            jq("#buttonBuy").attr("disabled", true);
            jq.getJSON({
                method: "POST",
                url: "/api/buy-cart",
                data: { cart: localStorage.getItem("cart") },
            })
                .done(function (data) {
                    localStorage.setItem("cart", null);
                    window.location = "/payments    ";
                })
                .fail(function (data) {
                    if (data && data.status == 401) {
                        var html = `<div class="alert alert-danger" role="alert">
        Please <a href="/login">Login</a> or <a href="/register">register</a> to buy
        </div>`;
                        jq("#errorCart").html(html);
                    }
                })
                .always(function () {
                    jq("#load-container").css({ display: "none" });
                    jq("#buttonBuy").attr("disabled", false);
                });
        });
        jq(".minus").click((event) => {
            var data = dataToJson(jq(event.target).attr("data"));
            var res = window.setQuantityProduct(data, -1);
            var key = jq(event.target).attr("key");
            jq(jq(".count")[key]).html(res);
        });
        jq(".plus").click((event) => {
            var data = dataToJson(jq(event.target).attr("data"));
            var res = window.setQuantityProduct(data, 1);
            var key = jq(event.target).attr("key");
            jq(jq(".count")[key]).html(res);
        });
    }

    // verify the cart and update global local storage
    async function verifyCart() {
        await jq
            .getJSON({
                method: "POST",
                url: "/api/verify-cart",
                data: { cart: JSON.stringify(cart) },
            })
            .done(function (data) {
                localStorage.setItem("cart", JSON.stringify(data));
                if (data && data.length > 0) {
                } else {
                    none_date();
                }
                rendered(data);
            })
            .fail(function (data) {})
            .always(function () {
                jq("#load-container").css({ display: "none" });
            });
    }
    verifyCart();
} else {
    none_date();
    //load page hidden
    jq("#load-container").css({ display: "none" });
}
// none data if empty cart show this message
function none_date() {
    jq("#containerCart").html(`
        <div class="alert alert-danger" role="alert">
        Your cart is empty
        </div>
        <a class="btn btn-info" href="/">Go Products</a>`);
}
