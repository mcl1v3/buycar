var jq = window.$;
let init = false;
//get query to the url
function getUrlParameter(sParam) {
    const params = new URLSearchParams(window.location.search);

    return params.get(sParam);
}
//get products to api and generate the products
async function getProducts(page = 1) {
    var search = getUrlParameter("search");
    var render = true;
    let string = "";
    if (search) {
        string += "search=" + search;
    }
    string += "&page=" + page;
    await jq
        .getJSON("/api/products?" + string)
        .then((data) => {
            if (data.data && data.data.length > 0) {
                generateProduct(data.data);
            } else {
                let html = `<br><br>
                <div class="alert alert-danger" role="alert">
                Not data found
                </div>`;
                jq("#containCard").html(html);
            }
            jq("#load-container").css({ display: "none" });
            var sources = [];
            for (let index = 1; index <= data.last_page; index++) {
                sources.push(index);
            }
            if (render && init) {
            } else {
                jq("#pagination-container").pagination({
                    dataSource: sources,
                    pageSize: 1,
                    pageNumber: page,
                    callback: function (data, pagination) {
                        if (init) {
                            getProducts(pagination.pageNumber);
                        } else {
                            init = true;
                        }
                    },
                });
            }
        })
        .catch((err) => {})
        .then(() => {
            render = false;
        });
}

// generate product logic html and set the add or minus to the cart
function generateProduct(data) {
    var html = "";

    data.forEach((el1, ind2) => {
        var tags = "";
        var details = "";
        el1.details.forEach((el2) => {
            details += `<span class="badge badge-pill badge-dark">${el2["detail"]["name"]} : ${el2["option"]["name"]}</span>`;
        });
        el1.tags.forEach((el2) => {
            tags += `<span  class="badge badge-pill badge-secondary">${el2["tag"]["name"]} </span> `;
        });
        var buttons = '<div class="text-center">Sin Stock</div>';
        if (el1.stock > 0) {
            buttons = `
        			<div class="qty mt-5 clicked-add">
                        <button  class="minus btn btn-sm btn-success"
                         data="${window.jsonToData(
                             el1
                         )}" key="${ind2}">-</button>
                        <span class="count">
                        ${window.getQuantityToProduct(el1.id)}</span>
                        <button class="plus btn btn-sm btn-success"
                        data="${window.jsonToData(
                            el1
                        )}" key="${ind2}">+</button>
                    </div>
        `;
        }
        html += `<div class="card col-sm-12 col-md-4 col-lg-3">
                <img class="card-img-top" src="https://picsum.photos/200/100?random=${
                    ind2 + 1
                }" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">
                        ${el1["name"]}
                    </h5>
                    <h6>Price $  ${el1["price"]} </h6>
                    <p class="card-text">
                      Description:   ${el1["description"]}
                    </p>
                    <p class="card-text">
                      Category:   ${el1["category"]["name"]}
                    </p>
                    <p class="card-text">
                        Details:
                            ${details}
                    </p>
                    <p class="card-text">
                        Tags:
                            ${tags}
                    </p>
                    <span>
                    ${buttons}
                    </span>
                </div>
            </div>`;
    });
    jq("#containCard").html(html);
    jq(".minus").click((event) => {
        var data = dataToJson(jq(event.target).attr("data"));
        var res = window.setQuantityProduct(data, -1);
        var key = jq(event.target).attr("key");
        jq(jq(".count")[key]).html(res);
    });
    jq(".plus").click((event) => {
        var data = dataToJson(jq(event.target).attr("data"));
        var res = window.setQuantityProduct(data, 1);
        var key = jq(event.target).attr("key");
        jq(jq(".count")[key]).html(res);
    });
}
getProducts();
