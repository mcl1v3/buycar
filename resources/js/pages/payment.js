import "select2";
var jq = window.$;

let init = false;
// show load container
jq("#load-container").css({ display: "flex" });
// payments get Payments
async function getPayments(page = 1) {
    let render = true;
    await jq
        .getJSON("/api/payment?page=" + page)
        .then((data) => {
            generatePayment(data.data);
            jq("#load-container").css({ display: "none" });
            var sources = [];
            for (let index = 1; index <= data.last_page; index++) {
                sources.push(index);
            }
            if (render && init) {
            } else {
                jq("#pagination-container").pagination({
                    dataSource: sources,
                    pageSize: 1,
                    pageNumber: page,
                    callback: function (data, pagination) {
                        if (init) {
                            getPayments(pagination.pageNumber);
                        } else {
                            init = true;
                        }
                    },
                });
            }
        })
        .catch((err) => {
            jq("#load-container").css({ display: "none" });
        })
        .then(() => {
            let render = false;
        });
}
// genreate the html code payment
function generatePayment(data) {
    var html = "";

    data.forEach((el1, ind2) => {
        html += `<tr>
        <td> ${el1.id}</td>
        <td> ${el1.product.name}</td>
        <td> ${el1.quantity}</td>
        <td> ${el1.pay}</td>
        <td> ${el1.created_at}</td>
        </tr>
        `;
    });
    jq("#body-table").html(html);
}

getPayments();
