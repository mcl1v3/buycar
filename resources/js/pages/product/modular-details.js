var jq = window.$;
// match text if ewual to the especific option
function matchText(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === "") {
        return data;
    }

    // Skip if there is no 'children' property
    if (typeof data.children === "undefined") {
        return null;
    }

    // `data.children` contains the actual options that we are matching against
    var filteredChildren = [];
    jq.each(data.children, function (idx, child) {
        if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
            filteredChildren.push(child);
        }
    });

    // If we matched any of the timezone group's children, then set the matched children on the group
    // and return the group object
    if (filteredChildren.length) {
        var modifiedData = jq.extend({}, data, true);
        modifiedData.children = filteredChildren;

        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}

var container = "containerDetails";
let selInput = jq("#selectDetails");
// select2 init to the especific
selInput.select2({
    placeholder: "Select an option",
    dropdownAutoWidth: true,
    allowClear: true,
    multiple: true,
    // templateResult: () => {},
    // templateSelection: () => {},
    matcher: matchText,
    ajax: {
        url: "/api/detail",
        dataType: "json",
        data: (param) => {
            console.log(param);
            var query = {
                search_all: param.term,
                page: param.page,
            };
            return query;
        },
        processResults: (data) => {
            var res = [];
            data.data.forEach((el1) => {
                res.push({
                    id: el1.id,
                    text:
                        el1.name +
                        " (" +
                        (el1.options ? el1.options.length : 0) +
                        " Options)",
                    options: el1.options,
                });
            });
            var more = false;
            if (data.last_page > data.current_page) {
                more = true;
            }
            return { results: res, pagination: { more } };
        },
    },
    theme: "classic",
    dropdownParent: jq("#modalAddEdit"),
    tags: true,
});
// wait event to select input and save and generate sub selects
selInput.on("select2:select", (val) => {
    var data = val.params.data;
    let options = [];
    if (data && data.options && data.options.length > 0) {
        data.options.forEach((el1) => {
            options.push({ id: el1.name, text: el1.name });
        });
    }
    let element = `<div id="detail-generate-${data.id}">
    <strong>${data.text}</strong><br>
     <select required id="selectDetail-${data.id}" class="form-control" name="product_detail_selected[${data.id}]">
              </select>
    </div>`;

    jq("#" + container).append(element);

    jq("#selectDetail-" + data.id).select2({
        placeholder: "Select an option",
        dropdownAutoWidth: true,
        allowClear: true,
        // templateResult: () => {},
        // templateSelection: () => {},
        matcher: matchText,
        theme: "classic",
        dropdownParent: jq("#modalAddEdit"),
        tags: true,
        data: options,
    });
});

// wait event to unselect input and save
selInput.on("select2:unselect", (val) => {
    var data = val.params.data;
    var id = "#detail-generate-" + data.id;
    jq("#" + container + " " + id).remove();
});
