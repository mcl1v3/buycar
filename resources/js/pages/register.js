var jq = window.$;

//load disabled
jq("#load-container").css({ display: "none" });
// register form set and send to the api
jq("#register").submit(function (event) {
    event.preventDefault();
    jq("#register button").attr("disabled", true);
    let form = {};
    jq("#register input,#register select,#register textarea").each(function (
        index
    ) {
        var input = jq(this);
        form[input.attr("name")] = input.val();
    });
    jq.getJSON({
        method: "POST",
        url: "/api/register",
        data: form,
    })
        .done(function (data) {
            window.location.reload();
        })
        .fail(function (data) {})
        .always(function () {
            jq("#register button").attr("disabled", false);
        });
});
