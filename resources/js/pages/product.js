var jq = window.$;
import swal from "sweetalert2";

// show load page

jq("#load-container").css({ display: "flex" });
let init = false;
// initial modal and disabled the keyboards
function initModal() {
    jq("#modal-add-stock").modal({ keyboard: false, backdrop: "static" });

    jq("#modal-add-stock .close").on("click", (val) => {
        jq("#modal-add-stock").modal("hide");
    });
    jq("#modalAddEdit").modal({ keyboard: false, backdrop: "static" });
    jq("#addProduct").on("click", (val) => {
        cleanParams();
        jq("#modalAddEdit").modal("show");
    });
    jq("#modalAddEdit .close").on("click", (val) => {
        jq("#modalAddEdit").modal("hide");
        cleanParams();
    });
}
function cleanParams() {
    jq(" #modalAddEdit input[name=id]").attr("value", null);
    jq(" #modalAddEdit input[name=name]").attr("value", null);
    jq(" #modalAddEdit textarea[name=description]").val(null);
    jq("#selectCategory").val(null).trigger("change");
    jq("#selectTag").val(null).trigger("change");
    jq("#selectDetails").val(null).trigger("change");
    jq(" #modalAddEdit input[name=price]").attr("value", null);
    jq(" #modalAddEdit input[name=hidden]").removeAttr("checked");
    jq("#containerDetails").html("");
}

initModal();
// get products to the api
async function getProducts(page = 1) {
    var perpage = 10;
    var render = true;
    await jq
        .getJSON("/api/product?per_page=" + perpage + "&page=" + page)
        .then((data) => {
            generateProduct(data.data);
            var sources = [];
            for (let index = 1; index <= data.last_page; index++) {
                sources.push(index);
            }
            if (render && init) {
            } else {
                jq("#pagination-container").pagination({
                    dataSource: sources,
                    pageSize: 1,
                    pageNumber: page,
                    callback: function (data, pagination) {
                        if (init) {
                            getProducts(pagination.pageNumber);
                        } else {
                            init = true;
                        }
                    },
                });
            }
            jq("#load-container").css({ display: "none" });
        })
        .catch((err) => {})
        .then(() => {
            render = false;
        });
}
// generate product html with the logic edit , delete, and add stock
function generateProduct(data) {
    var html = "";

    data.forEach((el1, ind2) => {
        var tags = "";
        var details = "";
        el1.details.forEach((el2) => {
            details += `<span  class="badge badge-pill badge-dark">${el2["detail"]["name"]} : ${el2["option"]["name"]}</span>`;
        });
        el1.tags.forEach((el2) => {
            tags += `<span class="badge badge-pill badge-secondary">${el2["tag"]["name"]} </span> `;
        });
        html += `<tr>
        <td> ${el1.id}</td>
        <td> ${el1.name}</td>
        <td> ${el1.description}</td>
        <td> ${el1.category.name}</td>
        <td> ${details}</td>
        <td> ${tags}</td>
        <td> ${el1.price}</td>
        <td> ${el1.stock}</td>
        <td> ${el1.hidden ? "Yes" : "No"}</td>
        <td> <button class="btn btn-sm btn-warning edit-action"  data="${window.jsonToData(
            el1
        )}">edit</button>
        <button class="btn btn-sm btn-danger delet-action" data="${window.jsonToData(
            el1
        )}">deleted</button>
        <button class="btn btn-sm btn-success add-stock"  data="${window.jsonToData(
            el1
        )}">add Stock</button></td>
        </tr>
        `;
    });
    jq("#body-table").html(html);
    jq(".delet-action").on("click", (val) => {
        var data = jq(val.target).attr("data");
        data = window.dataToJson(data);
        swal.fire({
            title: "Delete this product?",
            icon: "question",
            showCancelButton: true,
            confirmButtonText: "Delete",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                return jq
                    .getJSON({
                        method: "DELETE",
                        url: "/api/product/" + data.id,
                    })
                    .done(function (data) {
                        getProducts();
                    })
                    .fail(function (data) {})
                    .always(function () {});
            },
            allowOutsideClick: () => !swal.isLoading(),
        }).then((result) => {});
    });
    jq(".add-stock").on("click", (val) => {
        var data = jq(val.target).attr("data");
        data = window.dataToJson(data);
        jq("#modal-add-stock").modal("show");
        jq("#modal-add-stock #product_id").attr("value", data.id);
    });

    jq(".edit-action").on("click", (val) => {
        cleanParams();
        var data = jq(val.target).attr("data");
        data = window.dataToJson(data);
        jq(" #modalAddEdit input[name=id]").attr("value", data.id);
        jq(" #productId").html(data.id);
        jq(" #modalAddEdit input[name=name]").attr("value", data.name);
        jq(" #modalAddEdit textarea[name=description]").val(data.description);
        jq(" #modalAddEdit input[name=price]").attr("value", data.price);
        jq(" #modalAddEdit input[name=hidden]").attr(
            "checked",
            data.hidden == 1 ? true : false
        );

        if (data.category) {
            var newOption = new Option(
                data.category.name,
                data.category.id,
                true,
                true
            );
            // Append it to the select
            $("#selectCategory").append(newOption).trigger("change");
        }
        if (data.tags && data.tags.length > 0) {
            data.tags.forEach((el1) => {
                if (el1.tag && el1.tag.name) {
                    var newOption = new Option(
                        el1.tag.name,
                        el1.tag.id,
                        true,
                        true
                    );
                    // Append it to the select
                    $("#selectTag").append(newOption).trigger("change");
                }
            });
        }

        if (data.details && data.details.length > 0) {
            data.details.forEach((el1, ind22) => {
                if (el1.detail && el1.detail.name) {
                    var newOption = new Option(
                        el1.detail.name,
                        el1.detail.id,
                        true,
                        true
                    );
                    // Append it to the select
                    $("#selectDetails").append(newOption).trigger("change");

                    let element = `<div id="detail-generate-${el1.detail.id}">
    <strong>${el1.detail.name}</strong><br>
     <select required id="selectDetail-${el1.detail.id}" class="form-control" name="product_detail_selected[${el1.detail.id}]">
              </select>
    </div>`;

                    let options = [el1.detail.options];
                    var container = "containerDetails";
                    jq("#" + container).append(element);

                    jq("#selectDetail-" + el1.detail.id).select2({
                        placeholder: "Select an option",
                        dropdownAutoWidth: true,
                        allowClear: true,
                        // templateResult: () => {},
                        // templateSelection: () => {},
                        matcher: matchText,
                        theme: "classic",
                        dropdownParent: jq("#modalAddEdit"),
                        tags: true,
                        data: options,
                    });
                    var newOption = new Option(
                        el1.option.name,
                        el1.option.name,
                        true,
                        true
                    );
                    // Append it to the select
                    $("#selectDetail-" + el1.detail.id)
                        .append(newOption)
                        .trigger("change");
                }
            });
        }
        jq("#modalAddEdit").modal("show");
    });
}

getProducts();
// matchtext  is a function to search in all options values to the select2
function matchText(params, data) {
    // If there are no search terms, return all of the data
    if (jq.trim(params.term) === "") {
        return data;
    }

    // Skip if there is no 'children' property
    if (typeof data.children === "undefined") {
        return null;
    }

    // `data.children` contains the actual options that we are matching against
    var filteredChildren = [];
    jq.each(data.children, function (idx, child) {
        if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
            filteredChildren.push(child);
        }
    });

    // If we matched any of the timezone group's children, then set the matched children on the group
    // and return the group object
    if (filteredChildren.length) {
        var modifiedData = jq.extend({}, data, true);
        modifiedData.children = filteredChildren;

        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}
// init select2 to the especific select with ajax data
jq("#selectCategory").select2({
    placeholder: "Select an option",
    dropdownAutoWidth: true,
    allowClear: true,
    matcher: matchText,
    ajax: {
        url: "/api/category",
        dataType: "json",
        data: (param) => {
            var query = {
                search_all: param.term,
            };
            return query;
        },
        processResults: (data) => {
            var res = [];
            data.data.forEach((el1) => {
                res.push({ id: el1.id, text: el1.name });
            });
            var more = false;
            if (data.last_page > data.current_page) {
                more = true;
            }
            return { results: res, pagination: { more } };
        },
    },
    theme: "classic",
    dropdownParent: jq("#modalAddEdit"),
    tags: true,
});

// init select2 to the especific select with ajax data
jq("#selectTag").select2({
    placeholder: "Select an option",
    dropdownAutoWidth: true,
    allowClear: true,
    multiple: true,
    // templateResult: () => {},
    // templateSelection: () => {},
    matcher: matchText,
    ajax: {
        url: "/api/tag",
        dataType: "json",
        data: (param) => {
            console.log(param);
            var query = {
                search_all: param.term,
                page: param.page,
            };
            return query;
        },
        processResults: (data) => {
            var res = [];
            data.data.forEach((el1) => {
                res.push({ id: el1.id, text: el1.name });
            });
            var more = false;
            if (data.last_page > data.current_page) {
                more = true;
            }
            return { results: res, pagination: { more } };
        },
    },
    theme: "classic",
    dropdownParent: jq("#modalAddEdit"),
    tags: true,
});

// login to the addForm
jq("#addForm").submit(function (event) {
    event.preventDefault();
    let form = {};
    jq("#addForm input,#addForm select,#addForm textarea").each(function (
        index
    ) {
        var input = jq(this);
        if (input.attr("name") == "hidden") {
            form[input.attr("name")] = input[0].checked;
        } else {
            form[input.attr("name")] = input.val();
        }
    });
    jq.getJSON({
        method: "POST",
        url: "/api/product",
        data: form,
    })
        .done(function (data) {
            window.location.reload();
        })
        .fail(function (data) {})
        .always(function () {});
});
// submit to the stockform
jq("#stockForm").submit(function (event) {
    event.preventDefault();
    let form = {};
    jq("#stockForm input").each(function (index) {
        var input = jq(this);
        form[input.attr("name")] = input.val();
    });

    jq.getJSON({
        method: "POST",
        url: "/api/product-stock",
        data: form,
    })
        .done(function (data) {
            window.location.reload();
        })
        .fail(function (data) {})
        .always(function () {});
});

//modular details is to deatil config anidate
import("./product/modular-details");
