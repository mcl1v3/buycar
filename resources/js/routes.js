var path = window.location.pathname;

switch (path) {
    case "/login":
        import("./pages/login");
        break;
    case "/register":
        import("./pages/register");
        break;
    case "/cart":
        import("./pages/cart");
        break;
    case "/payments":
        import("./pages/payment");
        break;
    case "/admin":
        import("./pages/product");
        break;
    case "/":
        import("./pages/home");
        break;
    default:
        break;
}
