import $ from "jquery";
import "bootstrap";
import swal from "sweetalert2";
import "select2";
window.$ = window.jQuery = $;
$(document).ready(function () {
    require("paginationjs/dist/pagination");
});
/**
 * Ajax setup default error message to sweetalert
 */
$.ajaxSetup({
    error: function (x, status, error) {
        if (x.status !== 200) {
            var text = "";
            var data = x.responseJSON;
            if (data && data.errors && Object.keys(data.errors).length > 0) {
                console.log("pass error");
                Object.keys(data.errors).forEach((el1) => {
                    console.log(data.errors[el1]);
                    if (data.errors[el1] && data.errors[el1].length > 0) {
                        data.errors[el1].forEach((el2) => {
                            text += el2 + "<br>";
                        });
                    }
                });
            } else if (data && data.message) {
                text = data.message;
            }
            if (text) {
                swal.fire({
                    icon: "error",
                    title: "Oops...",
                    html: text,
                });
            }
            if (x.status == 403) {
                window.location.href = "/login";
            } else {
            }
        }
    },
});

// default global function to convert json to Data
window.jsonToData = (val) => {
    return encodeURIComponent(JSON.stringify(val));
};
// default global function to convert data to JSON
window.dataToJson = (val) => {
    return JSON.parse(decodeURIComponent(val));
};

// to the cart get Quantity per id especific product
window.getQuantityToProduct = (id) => {
    var cart = localStorage.getItem("cart");
    var quantity = 0;
    if (cart) {
        cart = JSON.parse(cart);
        if (cart) {
            cart.forEach((el1) => {
                if (el1.id == id) {
                    quantity = el1.quantity;
                }
            });
        }
    }
    return quantity;
};

// set quantity to the product
window.setQuantityProduct = (data, quantity) => {
    var cart = localStorage.getItem("cart");
    var newqua = 0;
    if (cart && cart != "null") {
        cart = JSON.parse(cart);
        if (cart && cart.length > 0) {
            var exist = false;
            cart.forEach((el1, ind) => {
                if (el1.id == data.id) {
                    exist = true;
                    newqua = cart[ind].quantity += quantity;
                }
            });
            if (!exist) {
                var tmp = data;
                newqua = tmp.quantity = quantity;
                cart.push(tmp);
            }
        } else {
            var tmp = data;
            newqua = tmp.quantity = quantity;
            cart = [tmp];
        }
        if (newqua >= 0 && data["stock"] >= newqua)
            localStorage.setItem("cart", JSON.stringify(cart));
        else newqua = cart[ind].quantity;
    } else {
        var tmp = data;
        newqua = tmp.quantity = quantity;
        cart = [tmp];
        if (newqua >= 0 && data["stock"] >= newqua)
            localStorage.setItem("cart", JSON.stringify(cart));
        else newqua = cart[ind].quantity;
    }
    window.updateCartGlobal();
    return newqua;
};
// update the global cart quantity
window.updateCartGlobal = () => {
    var cart = localStorage.getItem("cart");
    var newqua = 0;
    if (cart && cart != "null") {
        cart = JSON.parse(cart);
        cart.forEach((el1, ind) => {
            newqua += el1.quantity;
        });
    }
    $("#cart-counter").html(newqua);
};

window.updateCartGlobal();

// routes if necesary to the no reply the functions or logic and separate per page route
import("./routes");
