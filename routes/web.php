<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'getCourses']);
Route::get('/login', [HomeController::class, 'login']);
Route::get('/register', [HomeController::class, 'register']);
Route::get('/cart', [HomeController::class, 'cart']);
Route::middleware('auth')->group(
    function () {
        Route::get('/admin', [HomeController::class, 'cars']);
        Route::get('/payments', [HomeController::class, 'payments']);
    }
);

Route::get('logout', [LoginController::class, 'logout']);
/*function () {
    return view('welcome');
});*/
