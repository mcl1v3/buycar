<?php

use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\DetailController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::macro('apiTable', function ($uri, $controller) {
    // Route::get("{$uri}/active", [$controller, "active"])->name("{$uri}.active");
    Route::resource($uri, $controller);
    // Route::post('massive/' . $uri, [$controller, 'massiveEdit']);
    // Route::post('massive-delete/' . $uri, [$controller, 'massiveDestroy']);
});

Route::post('login', [LoginController::class, 'authenticate']);
Route::post('register', [LoginController::class, 'register']);
Route::post('verify-cart', [CartController::class, 'verifyCart']);
Route::get('products', [HomeController::class, 'products']);
//Route::get('product-filters', [HomeController::class, 'productFilter']);

//Route::get('user', function (Request $request) {
//    return $request->user();
//});
Route::middleware(['auth', 'role:1'])->group(function () {
    Route::apiTable('product', ProductController::class);
    Route::apiTable('category', CategoryController::class);
    Route::apiTable('tag', TagController::class);
    Route::apiTable('detail', DetailController::class);
    Route::post('product-stock', [ProductController::class, 'addStock']);
});

Route::middleware(['auth'])->group(
    function () {
        Route::post('buy-cart', [CartController::class, 'buyCart']);
        Route::get('payment', [PaymentController::class, 'get']);
    }
);
