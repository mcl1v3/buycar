<?php

namespace App\Models;

use App\Http\Controllers\Utils\ModelElasticSearch;
use App\Jobs\ElasticSearchSyncToModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Category extends ModelElasticSearch
{
    use HasFactory;
    use SoftDeletes;


    /**
     * The attributes that should be show for serialization.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'category_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
        'created_at',
    ];
}
