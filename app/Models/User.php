<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type_user_id'
    ];

    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($q) {
            $q->type_user_id = $q->type_user_id ?? 2;
        });
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * User define the setPayment set the Product and quantity
     */
    public function setPayment(Product $pro, $quantity)
    {
        $price = $pro['price'] * $quantity;
        $stock =  $pro->setStock($price, $quantity * -1);
        if ($stock) {
            Payment::create([
                'user_id' => $this->id,
                'pay' => $price,
                'discount' => 0,
                'cost' => $price,
                'stock_id' => $stock['id'],
                'product_id' => $pro['id'],
                'quantity' => $quantity
            ]);
        }
    }
}
