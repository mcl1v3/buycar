<?php

namespace App\Models;

use App\Http\Controllers\Utils\ModelElasticSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends ModelElasticSearch
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that should be show for serialization.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'price_buyed',
        'quantity',
        'product_id',
        'add_or_remove'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
        'created_at',
    ];
}
