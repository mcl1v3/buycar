<?php

namespace App\Models;

use App\Http\Controllers\Utils\ModelElasticSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends ModelElasticSearch
{
    use HasFactory;
    use SoftDeletes;
    /**
     * The attributes that should be show for serialization.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'user_id',
        'pay',
        'discount',
        'cost',
        'stock_id',
        'product_id',
        'quantity',
        'created_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * relation the payment by the product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
