<?php

namespace App\Models;

use App\Http\Controllers\Utils\ModelElasticSearch;
use App\Http\Traits\ElasticSearchTrait;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Product extends ModelElasticSearch
{
    use HasFactory;
    use ElasticSearchTrait;
    use SoftDeletes;

    /**
     * The attributes that should be show for serialization.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'price',
        'category_id',
        'user_id',
        'hidden',
        'created_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at',
    ];
    /**
     * Stock appends to other table
     */
    protected $appends = ['stock'];

    /**
     * Relation tags to product
     */
    public function tags()
    {
        return $this->hasMany(ProductTag::class);
    }
    /**
     * Relation category to product
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Relation get Details to product
     */
    public function details()
    {
        return $this->hasMany(ProductDetail::class, 'product_id');
    }

    /**
     * create or attach tag
     */
    public function setTag($name)
    {
        $tag = Tag::firstOrCreate(['name' => $name]);
        return ProductTag::firstOrCreate(['product_id' => $this->id, 'tag_id' => $tag['id']]);
    }
    /**
     * Attach tag to the product
     */
    public function linkTag(Tag $tag)
    {
        return ProductTag::firstOrCreate(['product_id' => $this->id, 'tag_id' => $tag['id']]);
    }
    /**
     * Set custom detailes
     */

    public function setDetail($name, $option)
    {
        $detail = Detail::whereName($name)->first();
        if (!$detail) {
            $detail = Detail::create(['name' => $name, 'options' => []]);
        }
        $options = $detail['options'];
        //exist
        $add = true;
        foreach ($options as $k => $v) {
            if ($v['name'] == $option) {
                $add = false;
            }
        }
        if ($add) {
            $options[] = ['name' => $option];
        }
        $detail->update(['options' => $options]);
        $pd = ProductDetail::whereProductId($this->id)->whereDetailId($detail['id'])->first();
        if ($pd) {
            $pd->update(['option' => ['name' => $option]]);
        } else {

            $pd = ProductDetail::firstOrCreate(['product_id' => $this->id, 'detail_id' => $detail['id'], 'option' => ['name' => $option]]);
        }
        return $pd;
    }
    /**
     * Get quantity Stock attribute
     */
    public function getStockAttribute()
    {
        $stock = Stock::whereProductId($this->id)->orderBy('id', 'desc')->first();
        if ($stock) {
            return $stock['quantity'];
        } else {
            return 0;
        }
    }
    /**
     * Add stock to Product
     */
    public function setStock($price, $quantity)
    {
        $addOrRemove = true;
        if ($quantity <= 0) $addOrRemove = false;

        $stock = Stock::whereProductId($this->id)->orderBy('id', 'desc')->first();
        $newed = 0;
        if ($stock) {
            $newed = $stock['quantity'] + $quantity;
        } else {
            $newed = $quantity;
        }
        if ($newed > 0) {
            return Stock::create(['price_buyed' => $price, 'product_id' => $this->id, 'add_or_remove' => $addOrRemove, 'quantity' => $newed]);
        } else {
            // throw new Exception("Not possible negative", 1);
            return null;
        }
    }
}
