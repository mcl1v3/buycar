<?php

namespace App\Jobs;

use App\Http\Controllers\Utils\ElasticSearchController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ElasticSearchSyncToModel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $model, $method;
    public function __construct($model, $method)
    {
        $this->method = $method;
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $data = $this->model;
        $method = $this->method;
        $class =  get_class($data);
        $ec = new ElasticSearchController();
        $ec->documentInit($class, $method, $data);
    }
}
