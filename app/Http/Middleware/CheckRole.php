<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        //if necesary to check role add ids or types of user separate by |
        $roles = explode('|', $role);
        foreach ($roles as $k => $v) {
            if ($request->user()->type_user_id == intval($v)) {
                return $next($request);
            }
        }

        // set not permitted here
        return response()->json(['error' => 'Not permitted enter here.'], 403);
    }
}
