<?php

namespace App\Http\Controllers\Utils;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class ElasticSearchController extends Controller
{
    //get the config project to the env files config
    public $url_elastic_search;
    //construct the controler
    public function __construct()
    {
        //genreate and define the structure the project
        $this->url_elastic_search = config('project.elasticsearch.scheme') . '://' . config('project.elasticsearch.host') . ':' . config('project.elasticsearch.port') . '/';
    }
    //document init to the especific function
    public function documentInit($class, $method, $data)
    {
        // structure to create file is defined by the url plus the class dir
        $name = \Str::lower(config('project.elasticsearch.data.prefix') . $this->clean($class));
        //elastic search if necesary create the file.json with the json
        $dir =  $this->createDocumentJson($data);

        try {
            //switch the method to the data set? create - update - delete with the respective POST, PUT , DELETE
            switch ($method) {
                case 'create':
                    $this->createDocument($data['id'], $name, $dir, 'POST');
                    break;
                case 'update':
                    # code...
                    $this->createDocument($data['id'], $name, $dir, 'PUT');
                    break;
                case 'delete':
                    $this->createDocument($data['id'], $name, $dir, 'DELETE');
                    # code...
                    break;
            }
            //code...
        } catch (\Throwable $th) {
            //only to problems show in the log
            Log::alert('ERROR WHEN ' . $method);
            Log::alert($th->getMessage());
        }
        //after create and send to the elastic search delete the file.json
        unlink($dir);
    }
    //documentSearch if create to search data in especific class model
    public function documentSearch($class, $options, $mode = 'array')
    {
        //generate the name
        $name = \Str::lower(config('project.elasticsearch.data.prefix') . $this->clean($class));
        //generate the file.json
        $dir =  $this->createDocumentJson(json_encode($options, 1));
        //_search if add to the url to search
        $data = $this->createDocument('_search', $name, $dir, 'GET');
        //delete file.json
        unlink($dir);
        //get the result
        $render = json_decode($data, 1);
        $res = [];
        // if exist results >0
        if (isset($render['hits']['hits']) && count($render['hits']['hits']) > 0) {
            //restructure the data to the logical internal pagination laravel
            foreach ($render['hits']['hits']  as $k => $v) {
                $res[] = $v['_source'];
            }
        }
        $realres = [];
        if ($mode == 'array') {
            $realres = $res;
        } else if ($mode == 'paginate') {
            $last_page = ($render['hits']['total']['value'] / $options['size']);
            if ($last_page > (int)$last_page) {
                $last_page = ((int)$last_page) + 1;
            } else {
                $last_page = (int)$last_page;
            }
            $realres = [
                'last_page' => $last_page,
                'data' => $res
            ];
        }
        //$res = ['data' => $res];
        return $realres;
    }
    // create Document if the action to the server elasticsearch
    public function createDocument($id, $name, $dir, $method = 'POST')
    {
        //using guzzle client to start
        $client = new Client();
        //define the method , sending the file and set the header json
        $send = [
            'body' => File::get($dir),
            'headers' => ['Content-Type' => 'application/json'],
        ];
        if ($method == 'delete' || $method == 'DELETE') {
            $send = [];
        }
        $res = $client->request($method, $this->url_elastic_search . $name . '/_doc/' . $id, $send);
        try {
            // if exist content return this
            return $res->getBody()->getContents();
        } catch (\Throwable $th) {
            // error internal to the test set in log
            Log::alert('Error when get content ');
            Log::alert($th->getMessage());
        }
        return [];
    }
    /**
     * create file json to send to elasticsearch
     */
    public function createDocumentJson($data)
    {

        $jsongFile = time() . '_file.json';
        $dir = storage_path('tmpjson/' . $jsongFile);
        File::put($dir, $data);
        return $dir;
    }

    //clean the logical values to 
    public function clean($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = str_replace('\\', '-', $string);
        $string = str_replace('/', '-', $string);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}
