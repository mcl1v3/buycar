<?php

namespace App\Http\Controllers\Utils;

use App\Http\Controllers\Controller;
use App\Jobs\ElasticSearchSyncToModel;
use Illuminate\Http\Request;

abstract class ApiTable extends Controller
{
    // global values protected
    protected $model;
    protected $val;
    protected $id;

    // this function execute before generate the Array object for the index function
    protected function queryAdd(Request $request, $q)
    {
        return $q;
    }

    // this function execute after generate the Array object for the index function
    protected function queryAddAfter(Request $request, $q)
    {
        return $q;
    }

    // index function, get pagination model to the global config
    public function index(Request $request)
    {
        //verify values
        $pp = $request->per_page > 0 ? $request->per_page : 0;
        $sort = $request->sort;
        $seall = $request->search_all;
        $seallcol = $request->search_columns;
        //for the model execute the default logic to the search values;
        $q = $this->model::where(function ($q) use ($seall, $seallcol) {
            if (isset($seall) && strlen($seall) > 0) {
                $concat = '';
                if (isset($seallcol) && count($seallcol) > 0) {
                    foreach ($seallcol as $k => $v) {
                        $concat .= 'IFNULL(' . $v . ',"")';
                        if (count($seallcol) - 1 != $k) {
                            $concat .= ',';
                        }
                    }
                } else {
                    $counter = 0;
                    foreach ($this->val as $k => $v) {
                        $concat .= 'IFNULL(' . $k . ',"")';
                        if (count($this->val) - 1 != $counter) {
                            $concat .= ',';
                        }
                        ++$counter;
                    }
                }
                $q->whereRaw('concat(' . $concat . ') LIKE ?', ['%' . $seall . '%']);
            }
        });
        //before generate the array or object call this function
        $q = $this->queryAdd($request, $q);
        // if defined  order by array sorting
        if (isset($sort) && count($sort) > 0) {
            foreach ($sort as $k => $v) {
                $q->orderBy($v['column'], $v['order']);
            }
        }
        //pagination create
        $q = $q->paginate($pp);
        //after generate array or object execute this function
        $q = $this->queryAddAfter($request, $q);

        return $q;
    }
    // default logic to store the model
    public function store(Request $request)
    {
        // only validate the values support
        $request->validate($this->getVal('create'));
        //get the params 
        $store = $this->model::create($request->all());
        //to default store elasticsearch create
        // \dispatch(new ElasticSearchSyncToModel($store, 'create'));
        return $store;
    }
    //get the data to the model by id
    public function show(Request $request, $id)
    {
        $q = $this->model::whereId($id);

        return $q->first();
    }
    // default logic to update
    public function update(Request $request, $id)
    {
        //validate
        $request->validate($this->getVal());
        //get all
        $res = $request->all();
        // if exist id in res remove
        unset($res['id']);
        //get the model
        $update = $this->model::whereId($id)->first();
        //update
        $res = $update->update($res);
        //update the elasticsearch doc
        //\dispatch(new ElasticSearchSyncToModel($update, 'update'));
        return $res;
    }
    //this logic massive edit to the one especific data, all update
    public function massiveEdit(Request $request)
    {
        //get all
        $res = $request->all();
        //get the array id to the res
        $id = $res['id'];
        // remove this id
        unset($res['id']);
        $val = ['id'];
        //filter the data to the val[]
        foreach ($res as $key => $value) {
            $val[] = $key;
        }
        // validation the keys exist and ids
        $request->validate($this->getVal($val, true));
        //get all ids coincident and update all
        return $this->model::whereIn('id', $id)->update($res);
    }
    // default destroy
    public function destroy($id)
    {
        //get the model
        $delete = $this->model::whereId($id)->first();
        //destroy model
        $res =   $delete->delete();
        //elastic search delete the doc
        //\dispatch(new ElasticSearchSyncToModel($delete, 'delete'));
        return $res;
    }
    //destroy arrays ids
    public function massiveDestroy(Request $request)
    {
        //disabled in testing mode
        return false;
        //get all
        $res = $request->all();
        //get all ids
        $id = $res['id'];
        //validate all data
        $request->validate($this->getVal(['id'], true));
        //delete all data
        return $this->model::whereIn('id', $id)->delete();
    }

    // get the config validation and generate the necessary validation to the inputs
    public function getVal($val = null, $massive = false)
    {
        $v = $this->val;
        if ($massive) {
            $v['id.*'] = $this->id;
        } else {
            $v['id'] = $this->id;
        }
        if ($val && is_array($val) && count($val) > 0) {
            $r = [];
            foreach ($val as $k => $v2) {
                foreach ($v as $k2 => $v3) {
                    if ($v2 == $k2) {
                        $r[$v2] = $v3;
                        break;
                    }
                }
            }

            return $r;
        } elseif ($val == 'create') {
            return $this->val;
        } else {
            return $v;
        }
    }
}
