<?php

namespace App\Http\Controllers\Utils;

use App\Jobs\ElasticSearchSyncToModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

abstract class ModelElasticSearch extends Model
{
    public static function boot()
    {
        parent::boot();
        static::created(function ($item) {
            Log::alert('CREATED');
            static::sync($item, 'create');
        });
        /*  static::saved(function ($item) {
            static::sync($item, 'update');
        });*/
        static::updated(function ($item) {
            Log::alert('UPDATE');
            static::sync($item, 'update');
        });
        static::deleting(function ($item) {
            Log::alert('DELETING');
            static::sync($item, 'delete');
        });
        /*
        static::trashed(function ($item) {
            static::sync($item, 'delete');
        });
        static::restored(function ($item) {
            static::sync($item, 'create');
        });*/
    }
    public  static function sync($item, $mode)
    {
        if ($item && $item['id']) {
            $model = get_called_class();
            $data = $model::whereId($item['id'])->first();
            \dispatch(new ElasticSearchSyncToModel(
                $data,
                $mode
            ));
        }
    }
}
