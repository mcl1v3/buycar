<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Utils\ApiTable;
use App\Http\Controllers\Utils\ElasticSearchController;
use App\Jobs\ElasticSearchSyncToModel;
use App\Models\Category;
use App\Models\Detail;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ProductTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class ProductController extends ApiTable
{
    //set the class model to the api table
    protected $model = Product::class;
    // set only values to verify and only get in request
    protected $val = [
        'name' => 'required|min:3',
        'description' => 'nullable|min:3',
        'price' => 'required|max:7',
        'category_id' => 'required',
        //'user_id' => 'required|exists:users,id',
        'hidden' => 'nullable',
    ];
    // set the id by default validation
    protected $id = 'required|exists:products,id';

    // api table add the with  params before the create
    public function queryAdd(Request $r, $q)
    {
        $q
            ->with(['details.detail', 'tags.tag', 'category'])->orderBy('created_at', 'desc');
        return $q;
    }

    //for the product default add stock
    public function addStock(Request $r)
    {
        $r->validate(['product_id' => 'required|exists:products,id', 'price' => 'required|max:7', 'quantity' => 'required']);
        $pro = Product::whereId($r->product_id)->first();
        $pro->setStock($r->price, $r->quantity);

        return [];
    }

    // api table by default not work fine, reescribe and create the store .
    public function store(Request $r)
    {
        //Validation
        $r->validate($this->getVal('create'));
        $all = $r->all();
        //if id exist go to update
        $pid = 0;
        if ($all['id']) {
            $pid = $all['id'];
        }
        //fix hidden boolean value
        if (isset($all['hidden'])) {
            if ($all['hidden'] == 'true') $all['hidden'] = 1;
            else if ($all['hidden'] == 'false') $all['hidden'] = 0;
            else
                $all['hidden'] = $all['hidden'] ? 1 : 0;
        }
        //get category if is value or number
        if (!($all['category_id'] > 0)) {
            $all['category_id'] = Category::create(['name' => $all['category_id']])->id;
        }
        // create the product
        if ($pid > 0) {
            $set = [
                'name' => $all['name'],
                'description' => $all['description'],
                'hidden' => $all['hidden'],
                'category_id' => $all['category_id'],
                'price' => $all['price'],
            ];
            $store = $this->model::whereId($all['id'])->first();
            $store->update($set);
        } else {
            $store = $this->model::create($all);
        }
        //Verify if tags created
        if (isset($r->product_tag)) {
            $tag = $r->product_tag;
            //Foreach the tags send
            if ($pid) {
                ProductTag::whereProductId($store->id)->whereNotIn('tag_id', $tag)->delete();
            }
            foreach ($tag as $k => $v) {
                //if >0 link to the exist Tag Class
                if ($v > 0) {
                    $store->linkTag(Tag::whereId($v)->first());
                } else {
                    //if not exist create tag
                    $store->setTag($v);
                }
            }
        }
        //product details exist?
        if (isset($r->product_details)) {
            $pd = $r->product_details;
            //product details foreach
            if ($pid) {
                ProductDetail::whereProductId($store->id)->whereNotIn('detail_id', $pd)->delete();
            }
            foreach ($pd as $k => $v) {
                $det = ['name' => $v];
                // if detail exist id?, get
                if ($v > 0) {
                    $det = Detail::whereId($v)->first();
                }
                //and set detail to the product
                if (isset($r->product_detail_selected[$v]))
                    $store->setDetail($det['name'], $r->product_detail_selected[$v]);
            }
        }
        //for the create ElasticSearch create and sync with the document
        \dispatch(new ElasticSearchSyncToModel(
            Product::whereId($store['id'])
                ->with(['details.detail', 'tags.tag', 'category'])
                ->first(),
            'create'
        ));
        return $store;
    }
}
