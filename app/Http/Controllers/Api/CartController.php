<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Verification the localstorage cart for all user, auth or not auth
     */
    public function verifyCart(Request $r, $changed = false)
    {
        //validation
        $r->validate(['cart' => 'required']);
        try {
            $cart = $r->cart;
            $cart = json_decode($cart, 1);
            $new = [];
            $quantity = 0;
            $lastquantity = 0;
            //foreach products verification
            foreach ($cart as $k => $v) {
                //if exist if not hidden
                $p = Product::whereId($v['id'])->with(['details.detail', 'tags.tag', 'category'])->whereHidden(false)->first();
                if ($p) {

                    $p = $p->toArray();
                    //get the quantity real
                    if ($v['quantity'] == 0) {
                    } else {
                        if ($p['stock'] > $v['quantity'] && $p['stock'] > 0) {

                            $p['quantity'] = $v['quantity'];
                            $new[] = $p;
                        } else if ($p['stock'] > 0) {
                            $p['quantity'] = $p['stock'];
                            $new[] = $p;
                        }
                    }
                    $quantity += $p['quantity'];
                }
                $lastquantity += $v['quantity'];
            }
            //verify the change quantity total
            if ($changed) {
                if ($quantity != $lastquantity) {
                    return [$new, true];
                } else {
                    return [$new, false];
                }
            }
            //code...
            return $new;
        } catch (\Throwable $th) {
            abort(522, $th->getMessage());
        }
        # code...
    }


    /**
     * Buy Cart, first verify cart and next set the payments
     */
    public function buyCart(Request $r)
    {
        //verify the cart
        $cart = $this->verifyCart($r, true);
        if ($cart[1]) {
            abort(522, 'Cart Values Changed, verify changes');
        }
        //get user
        $user = auth()->user();
        if ($cart[0] && $cart[0] > 0) {
            //cart correct verification create the payments by product
            foreach ($cart[0] as $k => $v) {
                $pro = Product::whereId($v['id'])->first();
                if ($pro)
                    $user->setPayment($pro, $v['quantity']);
            }
        }
        return [];
    }
}
