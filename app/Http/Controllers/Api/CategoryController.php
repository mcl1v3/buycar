<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Utils\ApiTable;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends ApiTable
{
    //set the class model to the api table
    protected $model = Category::class;
    // set only values to verify and only get in request
    protected $val = [
        'name' => 'required|min:3',
        'description' => 'nullable|min:3',
        'category_id' => 'nullable',
    ];
    // set the id by default validation
    protected $id = 'required|exists:categories,id';
}
