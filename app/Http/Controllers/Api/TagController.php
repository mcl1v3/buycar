<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Utils\ApiTable;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends ApiTable
{
    //
    //set the class model to the api table
    protected $model = Tag::class;
    // set only values to verify and only get in request
    protected $val = [
        'name' => 'required|min:3',
        'description' => 'nullable|min:3',
    ];
    // set the id by default validation
    protected $id = 'required|exists:tags,id';
}
