<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    // for all user auth get the payments
    public function get(Request $r)
    {
        $u = $r->user();
        if ($u) {
            return Payment::whereUserId($u['id'])->with('product')->paginate(12);
        }
    }
}
