<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //login default logic
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return ['message' => 'Login Successfull'];
        }

        return response()->json([
            'message' => 'Email or Password Invalid.',
        ], 522);
    }
    //logout default logic
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
    //register and automatic log in > default logic
    public function register(Request $r)
    {
        $r->validate(
            ['name' => 'required|min:3', 'email' => 'required|email|unique:users,email', 'password' => 'required|min:4|confirmed']
        );
        $data = $r->all();
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $credentials = $r->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $r->session()->regenerate();

            return ['message' => 'Register Successfull'];
        }

        return response()->json([
            'message' => 'Invalid Params.',
        ], 522);
    }
}
