<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Utils\ElasticSearchController;
use App\Models\Product;
use App\SearchRepository;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // for testing details verify if authtype with the type user
    public function authType($type = 2)
    {
        $auth = auth()->user();
        if ($auth) {
            if ($auth->type_user_id == $type) {
                return true;
            }
        }
        return false;
    }
    // generate the view product
    public function getCourses()
    {
        return view('products');
    }
    // login view default if exist auth redirect to the home
    public function login()
    {
        if (\Auth::check()) {
            return redirect('/');
        }
        return view('auth.login');
    }
    // register view default if exist auth redirect to home
    public function register()
    {
        if (\Auth::check()) {
            return redirect('/');
        }
        return view('auth.register');
    }

    //cart view
    public function cart()
    {
        return view('user.cart');
    }

    // home require the product search in base the elastic search logical
    public function products(Request $r)
    {
        $perpage = $r->per_page > 0 ? $r->per_page : 12;
        $page = $r->page > 0 ? $r->page : 1;
        //if search exist or set one letter
        if (isset($r->search)) {
            //call the elastic search controller
            $esea = new ElasticSearchController();
            // search 
            return  $esea->documentSearch(
                get_class(new Product),
                [
                    "sort" => [
                        ["created_at" => ["order" => "desc", "format" => "strict_date_optional_time_nanos"]],
                    ],
                    'query' => [
                        "bool" => [
                            "must" => [
                                [
                                    'multi_match' => [
                                        "query" => isset($r->search) ? $r->search : '',
                                        "fields" => [
                                            'name',
                                            'description',
                                            'details*.option.name',
                                            'tags*.tag.name',
                                            'category.name'
                                        ],
                                        "operator" => "or",
                                        "type" => "bool_prefix"
                                    ],
                                ], [
                                    'match' => [
                                        'hidden' => 0
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "size" => $perpage,
                    "from" => ($page - 1) * $perpage
                ],
                'paginate',
            );
        }
        //set the default products
        return Product::with(['details.detail', 'tags.tag', 'category'])->whereHidden(false)->orderBy('created_at', 'desc')->paginate($perpage);
    }

    //filters to the products in home page
    public function productFilter(Request $r)
    {
        /* if (isset($r->search)) {
        } else {
        }*/
    }
    //view cars default only for auth and user type 1
    public function cars()
    {
        if (!$this->authType(1))
            return redirect('/login');
        return view('admin.cars');
    }
    // view payments only for auth users
    public function payments()
    {
        if (\Auth::check()) {
            return view('admin.payments');
        }
        return redirect('/login');
    }
}
