<?php

namespace App\Console\Commands;

use App\Jobs\ElasticSearchSyncToModel;
use App\Models\Product;
use Illuminate\Console\Command;

class SyncProductElasticSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pro = Product::with(['details.detail', 'tags.tag', 'category'])->get();
        if ($pro && count($pro) > 0) {
            foreach ($pro as $k => $v) {
                \dispatch(new ElasticSearchSyncToModel(
                    $v,
                    'update'
                ));
            }
        }
        return 0;
    }
}
