<?php

/**
 * Define global values to the project and get config('project.*)
 * if necesary to use the php artisan optimize
 */
return [
    'elasticsearch' => [
        'host' => env('ELASTICSEARCH_HOST', 'localhost'),
        'port' => env('ELASTICSEARCH_PORT', '9200'),
        'scheme' => env('ELASTICSEARCH_SCHEME', 'http'),
        'user' => env('ELASTICSEARCH_USER'),
        'pass' => env('ELASTICSEARCH_PASS'),
        'data' => [
            'prefix' => env('ELASTICSEARCH_PREFIX', '')
        ]
    ]
];
