<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('tags', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('details', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('product_details', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('stocks', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('product_tags', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('tags', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('details', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('product_details', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('stocks', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('product_tags', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
