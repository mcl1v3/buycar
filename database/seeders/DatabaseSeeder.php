<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Detail;
use App\Models\Product;
use App\Models\Tag;
use App\Models\TypeUser;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        // create default type of user
        $sad =  TypeUser::create(['name' => 'superadmin']);
        TypeUser::create(['name' => 'admin']);
        TypeUser::create(['name' => 'guest']);

        //generate default user
        $uadmin = User::create(['name' => 'Cesar Salvatierra', 'email' => 'mclive.case@gmail.com', 'password' => bcrypt('admin'), 'type_user_id' => $sad['id']]);
        $uadmin = User::create(['name' => 'Admin', 'email' => 'admin@gmail.com', 'password' => bcrypt('admin'), 'type_user_id' => $sad['id']]);

        for ($i = 1; $i < rand(10, 30); $i++) {

            Category::create(['name' => $faker->firstName, 'description' => $faker->text(400)]);
        }

        for ($i = 1; $i < rand(40, 100); $i++) {

            Tag::create(['name' => $faker->word, 'description' => $faker->text(400)]);
        }

        for ($i = 1; $i < rand(30, 60); $i++) {
            $arr = [];
            // for ($i = 1; $i < rand(5, 10); $i++) {
            //     $arr[] = ['name' => $faker->word];
            // }
            Detail::create([
                'name' => $faker->word,
                'description' => $faker->text(400),
                'options' => $arr
            ]);
        }


        for ($i = 1; $i < rand(40, 100); $i++) {
            $pro =  Product::create([
                'user_id' => $uadmin['id'],
                'name' => $faker->name,
                'description' => $faker->realText(500),
                'price' => $faker->randomDigit(),
                'category_id' => Category::inRandomOrder()->first()['id'],
            ]);
            $randomTags = Tag::inRandomOrder()->take(5)->get();
            foreach ($randomTags as $k => $v) {
                $pro->linkTag($v);
            }
            $randDetail = Detail::inRandomOrder()->take(5)->get();
            foreach ($randDetail as $k => $v) {
                $pro->setDetail($v['name'], $faker->word);
            }
            $pro->setStock($faker->randomDigit(5), rand(100, 500));
        }
        for ($i = 0; $i <= 40; $i++) {
            $u = User::create(['name' => $faker->name, 'email' => $faker->email, 'password' => bcrypt($faker->name)]);
            for ($i2 = 0; $i2 < 5; $i2++) {
                $pro = Product::inRandomOrder()->first();

                $u->setPayment($pro, rand(1, 10));
            }
        }
        // \App\Models\User::factory(10)->create();
    }
}
