(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_pages_product_js"],{

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./resources/js/pages/product.js":
/*!***************************************!*\
  !*** ./resources/js/pages/product.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var jq = window.$;
 // show load page

jq("#load-container").css({
  display: "flex"
});
var init = false; // initial modal and disabled the keyboards

function initModal() {
  jq("#modal-add-stock").modal({
    keyboard: false,
    backdrop: "static"
  });
  jq("#modal-add-stock .close").on("click", function (val) {
    jq("#modal-add-stock").modal("hide");
  });
  jq("#modalAddEdit").modal({
    keyboard: false,
    backdrop: "static"
  });
  jq("#addProduct").on("click", function (val) {
    cleanParams();
    jq("#modalAddEdit").modal("show");
  });
  jq("#modalAddEdit .close").on("click", function (val) {
    jq("#modalAddEdit").modal("hide");
    cleanParams();
  });
}

function cleanParams() {
  jq(" #modalAddEdit input[name=id]").attr("value", null);
  jq(" #modalAddEdit input[name=name]").attr("value", null);
  jq(" #modalAddEdit textarea[name=description]").val(null);
  jq("#selectCategory").val(null).trigger("change");
  jq("#selectTag").val(null).trigger("change");
  jq("#selectDetails").val(null).trigger("change");
  jq(" #modalAddEdit input[name=price]").attr("value", null);
  jq(" #modalAddEdit input[name=hidden]").attr("value", false);
  jq(" #modalAddEdit input[name=hidden]").removeAttr("checked");
  jq("#containerDetails").html("");
}

initModal(); // get products to the api

function getProducts() {
  return _getProducts.apply(this, arguments);
} // generate product html with the logic edit , delete, and add stock


function _getProducts() {
  _getProducts = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
    var page,
        perpage,
        render,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            page = _args.length > 0 && _args[0] !== undefined ? _args[0] : 1;
            perpage = 10;
            render = true;
            _context.next = 5;
            return jq.getJSON("/api/product?per_page=" + perpage + "&page=" + page).then(function (data) {
              generateProduct(data.data);
              var sources = [];

              for (var index = 1; index <= data.last_page; index++) {
                sources.push(index);
              }

              if (render && init) {} else {
                jq("#pagination-container").pagination({
                  dataSource: sources,
                  pageSize: 1,
                  pageNumber: page,
                  callback: function callback(data, pagination) {
                    if (init) {
                      getProducts(pagination.pageNumber);
                    } else {
                      init = true;
                    }
                  }
                });
              }

              jq("#load-container").css({
                display: "none"
              });
            })["catch"](function (err) {}).then(function () {
              render = false;
            });

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getProducts.apply(this, arguments);
}

function generateProduct(data) {
  var html = "";
  data.forEach(function (el1, ind2) {
    var tags = "";
    var details = "";
    el1.details.forEach(function (el2) {
      details += "<span  class=\"badge badge-pill badge-dark\">".concat(el2["detail"]["name"], " : ").concat(el2["option"]["name"], "</span>");
    });
    el1.tags.forEach(function (el2) {
      tags += "<span class=\"badge badge-pill badge-secondary\">".concat(el2["tag"]["name"], " </span> ");
    });
    html += "<tr>\n        <td> ".concat(el1.id, "</td>\n        <td> ").concat(el1.name, "</td>\n        <td> ").concat(el1.description, "</td>\n        <td> ").concat(el1.category.name, "</td>\n        <td> ").concat(details, "</td>\n        <td> ").concat(tags, "</td>\n        <td> ").concat(el1.price, "</td>\n        <td> ").concat(el1.stock, "</td>\n        <td> ").concat(el1.hidden ? "Yes" : "No", "</td>\n        <td> <button class=\"btn btn-sm btn-warning edit-action\"  data=\"").concat(window.jsonToData(el1), "\">edit</button>\n        <button class=\"btn btn-sm btn-danger delet-action\" data=\"").concat(window.jsonToData(el1), "\">deleted</button>\n        <button class=\"btn btn-sm btn-success add-stock\"  data=\"").concat(window.jsonToData(el1), "\">add Stock</button></td>\n        </tr>\n        ");
  });
  jq("#body-table").html(html);
  jq(".delet-action").on("click", function (val) {
    var data = jq(val.target).attr("data");
    data = window.dataToJson(data);
    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
      title: "Delete this product?",
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "Delete",
      cancelButtonText: "No",
      showLoaderOnConfirm: true,
      preConfirm: function preConfirm(login) {
        return jq.getJSON({
          method: "DELETE",
          url: "/api/product/" + data.id
        }).done(function (data) {
          getProducts();
        }).fail(function (data) {}).always(function () {});
      },
      allowOutsideClick: function allowOutsideClick() {
        return !sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().isLoading();
      }
    }).then(function (result) {});
  });
  jq(".add-stock").on("click", function (val) {
    var data = jq(val.target).attr("data");
    data = window.dataToJson(data);
    jq("#modal-add-stock").modal("show");
    jq("#modal-add-stock #product_id").attr("value", data.id);
  });
  jq(".edit-action").on("click", function (val) {
    cleanParams();
    var data = jq(val.target).attr("data");
    data = window.dataToJson(data);
    jq(" #modalAddEdit input[name=id]").attr("value", data.id);
    jq(" #productId").html(data.id);
    jq(" #modalAddEdit input[name=name]").attr("value", data.name);
    jq(" #modalAddEdit textarea[name=description]").val(data.description);
    jq(" #modalAddEdit input[name=price]").attr("value", data.price);
    if (data.hidden == 1) jq(" #modalAddEdit input[name=hidden]").attr("checked", "checked");

    if (data.category) {
      var newOption = new Option(data.category.name, data.category.id, true, true); // Append it to the select

      $("#selectCategory").append(newOption).trigger("change");
    }

    if (data.tags && data.tags.length > 0) {
      data.tags.forEach(function (el1) {
        if (el1.tag && el1.tag.name) {
          var newOption = new Option(el1.tag.name, el1.tag.id, true, true); // Append it to the select

          $("#selectTag").append(newOption).trigger("change");
        }
      });
    }

    if (data.details && data.details.length > 0) {
      data.details.forEach(function (el1, ind22) {
        if (el1.detail && el1.detail.name) {
          var newOption = new Option(el1.detail.name, el1.detail.id, true, true); // Append it to the select

          $("#selectDetails").append(newOption).trigger("change");
          var element = "<div id=\"detail-generate-".concat(el1.detail.id, "\">\n    <strong>").concat(el1.detail.name, "</strong><br>\n     <select required id=\"selectDetail-").concat(el1.detail.id, "\" class=\"form-control\" name=\"product_detail_selected[").concat(el1.detail.id, "]\">\n              </select>\n    </div>");
          var options = [el1.detail.options];
          var container = "containerDetails";
          jq("#" + container).append(element);
          jq("#selectDetail-" + el1.detail.id).select2({
            placeholder: "Select an option",
            dropdownAutoWidth: true,
            allowClear: true,
            // templateResult: () => {},
            // templateSelection: () => {},
            matcher: matchText,
            theme: "classic",
            dropdownParent: jq("#modalAddEdit"),
            tags: true,
            data: options
          });
          var newOption = new Option(el1.option.name, el1.option.name, true, true); // Append it to the select

          $("#selectDetail-" + el1.detail.id).append(newOption).trigger("change");
        }
      });
    }

    jq("#modalAddEdit").modal("show");
  });
}

getProducts(); // matchtext  is a function to search in all options values to the select2

function matchText(params, data) {
  // If there are no search terms, return all of the data
  if (jq.trim(params.term) === "") {
    return data;
  } // Skip if there is no 'children' property


  if (typeof data.children === "undefined") {
    return null;
  } // `data.children` contains the actual options that we are matching against


  var filteredChildren = [];
  jq.each(data.children, function (idx, child) {
    if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
      filteredChildren.push(child);
    }
  }); // If we matched any of the timezone group's children, then set the matched children on the group
  // and return the group object

  if (filteredChildren.length) {
    var modifiedData = jq.extend({}, data, true);
    modifiedData.children = filteredChildren; // You can return modified objects from here
    // This includes matching the `children` how you want in nested data sets

    return modifiedData;
  } // Return `null` if the term should not be displayed


  return null;
} // init select2 to the especific select with ajax data


jq("#selectCategory").select2({
  placeholder: "Select an option",
  dropdownAutoWidth: true,
  allowClear: true,
  matcher: matchText,
  ajax: {
    url: "/api/category",
    dataType: "json",
    data: function data(param) {
      var query = {
        search_all: param.term
      };
      return query;
    },
    processResults: function processResults(data) {
      var res = [];
      data.data.forEach(function (el1) {
        res.push({
          id: el1.id,
          text: el1.name
        });
      });
      var more = false;

      if (data.last_page > data.current_page) {
        more = true;
      }

      return {
        results: res,
        pagination: {
          more: more
        }
      };
    }
  },
  theme: "classic",
  dropdownParent: jq("#modalAddEdit"),
  tags: true
}); // init select2 to the especific select with ajax data

jq("#selectTag").select2({
  placeholder: "Select an option",
  dropdownAutoWidth: true,
  allowClear: true,
  multiple: true,
  // templateResult: () => {},
  // templateSelection: () => {},
  matcher: matchText,
  ajax: {
    url: "/api/tag",
    dataType: "json",
    data: function data(param) {
      console.log(param);
      var query = {
        search_all: param.term,
        page: param.page
      };
      return query;
    },
    processResults: function processResults(data) {
      var res = [];
      data.data.forEach(function (el1) {
        res.push({
          id: el1.id,
          text: el1.name
        });
      });
      var more = false;

      if (data.last_page > data.current_page) {
        more = true;
      }

      return {
        results: res,
        pagination: {
          more: more
        }
      };
    }
  },
  theme: "classic",
  dropdownParent: jq("#modalAddEdit"),
  tags: true
}); // login to the addForm

jq("#addForm").submit(function (event) {
  event.preventDefault();
  var form = {};
  jq("#addForm input,#addForm select,#addForm textarea").each(function (index) {
    var input = jq(this);
    form[input.attr("name")] = input.val();
  });

  if (form["hidden"]) {
    form["hidden"] = form["hidden"];
  }

  jq.getJSON({
    method: "POST",
    url: "/api/product",
    data: form
  }).done(function (data) {
    window.location.reload();
  }).fail(function (data) {}).always(function () {});
}); // submit to the stockform

jq("#stockForm").submit(function (event) {
  event.preventDefault();
  var form = {};
  jq("#stockForm input").each(function (index) {
    var input = jq(this);
    form[input.attr("name")] = input.val();
  });
  jq.getJSON({
    method: "POST",
    url: "/api/product-stock",
    data: form
  }).done(function (data) {
    window.location.reload();
  }).fail(function (data) {}).always(function () {});
}); //modular details is to deatil config anidate

__webpack_require__.e(/*! import() */ "resources_js_pages_product_modular-details_js").then(__webpack_require__.t.bind(__webpack_require__, /*! ./product/modular-details */ "./resources/js/pages/product/modular-details.js", 23));

/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/***/ ((module) => {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : 0
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}


/***/ })

}]);