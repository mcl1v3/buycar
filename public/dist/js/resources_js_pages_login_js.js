(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_pages_login_js"],{

/***/ "./resources/js/pages/login.js":
/*!*************************************!*\
  !*** ./resources/js/pages/login.js ***!
  \*************************************/
/***/ (() => {

var jq = window.$; // load page hidden

jq("#load-container").css({
  display: "none"
}); // login form submit logic

jq("#loginForm").submit(function (event) {
  event.preventDefault();
  var stats = {
    email: jq("#inputEmail").val(),
    password: jq("#inputPassword").val()
  };
  jq("#loginForm button").attr("disabled", true);
  jq.getJSON({
    method: "POST",
    url: "/api/login",
    data: stats
  }).done(function (data) {
    window.location.reload();
  }).fail(function (data) {
    jq("#errorsLogin").css("display", "block");
    jq("#errorsLogin").html("Error con las credenciales");
  }).always(function () {
    jq("#loginForm button").attr("disabled", false);
  });
});

/***/ })

}]);